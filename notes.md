# Parallel Programming

Konsep / Solusi dalam memecahkan sebuah masalah dengan cara membagi masalah terkait menjadi beberapa bagian lalu dijalankan / diselesaikan secara bersamaan pada waktu yang bersamaan pula

## Contoh

- Menjalankan beberapa aplikasi sekaligus di Laptop / PC kita (Buka VsCode, google chrome dan terminal dalam waktu yang bersamaan)
- Antrian di bank yang teller > 1 dimana masing-masing teller melayani nasabah dalam waktu yang bersamaan

Parallel Programming sangat erat kaitannya dengan Process dan Thread. Berikut Perbedaannya:

| Process                              | Thread                                         |
| ------------------------------------ | ---------------------------------------------- |
| Sebuah Eksekusi Program              | Merupakan Segmen Dari Proses                   |
| Konsumsi Memori Besar                | Konsumsi Memori Kecil                          |
| Terisolasi Antar Proses              | Saling Berhubungan Jika Dalam Proses Yang Sama |
| Lama Untuk Dijalankan dan Dihentikan | Cepat Untuk Dijalankan Dan Dihentikan          |

# Concurrency Programming

Konsep / Solusi dalam memecahkan sebuah masalah dengan cara menjalankan menjalankan beberapa pekerjaan secara pergantian. Concurrency hanya membutuhkan sedikit thread ketimbang Parallel Programming

## Contoh

- Manusia dalam menjalankan kesehariannya banyak melakukan hal. Manusia tidak mungkin bisa mengerjakan pekerjaan dalam satu waktu secara beriringan

# Konsep CPU Bound dan I/O Bound

| CPU Bound                                                                                          | I/O Bound                                                                                                                                           |
| -------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| Aplikasi yang banyak menjalankan algoritma dimana prosesnya sangat bergantung dengan kecepatan CPU | Aplikasi yang banyak menjalankan algoritma dimana prosesnya sangat bergantung dengan kecepatan input / output devices yang digunakan (Ram, Storage) |
| Contoh: Machine Learning                                                                           | Contoh: Membaca data dari file, database dan lain sebagainya                                                                                        |

```
Golang sendiri sebetulnya bahasa pemograman yang berbasis Concurrency Programming secara defaultnya. Namun demikian, Golang juga bisa diubah modenya menjadi Parallel Programming (Goroutines).

Maka dari itu Golang sangat Powerfull apabila digunakan untuk Web Development.

Apabila ingin membuat aplikasi yang berbasis AI seperti Machine Learning, Golang sebetulnya kurang tepat untuk dipilih sebagai bahas pemogramannya. Ada banyak alternatif yang lebih baik seperti Python, Java dan lain sebagainya.

```

# Goroutine

Goroutine merupakan thread ringan yang dikelola oleh Go Runtime. Dikatakan ringan karena untuk membuat sebua Goroutine hanya dibutuhkan memori sebesar 2 kb. Untuk informasi kalau Thread membutuhkan minimal kurang lebih 1 mb / 1000 kb hanya untuk membuat 1 Thread. Jadi secara kasar bisa dibilang 500 Goroutine setara 1 Thread Normal. Inilah salah satu alasan yang membuat Golang powerfull.

> Meskipun berjalan diatas Thread, namun sebetulnya cara kerja Goroutine itu Concurrency bukan Parallel.

Seperti yang sudah dibahas diatas, Goroutine dikelola oleh Go Runtime. Lebih spesifiknya Go Runtime memiliki fitur / platform yang bernama Go Scheduler yang fungsinya untuk mengelola GoRoutine.

Goroutine pada dasarnya proses kecil yang berjalan di atas Thread asli. Keuntungan Goroutine selain penggunaan memori yang kecil, kita sudah tidak perlu lagi melakukan manajemen Thread yang biasa kita lakukan pada Bahasa pemograman lain. Go Runtime melalui fitu go Scheduler sudah dengan otomatis akan membantu kita dalam mengelola si Goroutine ini.
