package service

import "fmt"

func HelloWorld(name string) {
	fmt.Println("Hello", name)
}
