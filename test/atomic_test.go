package test

import (
	"fmt"
	"sync"
	"sync/atomic"
	"testing"
)

/*
* Atomic merupakan package yang digunakan untuk mengeksekusi data primitive secara aman dalam proses concurent (Goroutine)
* Sebelumnya, digunakan mutex untuk mencegah terjadinya sebuah race condition ketika melakukan manipulasi data pada data primitive
* Dengan menggunakan atomic, kita tidak perlu lagi menggunakan Mutex untuk menanggulangi masalah race condition pada tipe data primitive
 */
func TestAtomic(t *testing.T) {
	var x int64 = 0

	var group sync.WaitGroup = sync.WaitGroup{}

	for i := 1; i <= 1000; i++ {
		group.Add(1)

		go func() {
			defer group.Done()

			for j := 1; j <= 2; j++ {
				atomic.AddInt64(&x, 1)
			}
		}()
	}

	group.Wait()

	fmt.Println("Counter =", x)
}
