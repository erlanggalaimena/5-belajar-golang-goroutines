package test

import (
	"fmt"
	"strconv"
	"testing"
	"time"
)

/*
* Channel merupakan tempat komunikasi secara synchronus yang digunakan oleh goroutines untuk melakukan pertukaran data
* Dalam Channel ada penerima dan pengirim
* Maksudnya adalah ketika kita menggunakan channel, pertama kali kita pasti mengirimkan data ke Channel terkait maka harus ada penerima data dari Channel terkait
* Apabila salah satunya tidak ada baik pengirim maupun penerima, maka akan terjadi error
* Apabila sebuah goroutines menggunakan channel, maka prosesnya akan terblock sampai data dari channel tersebut digunakan. Maka dari itu disebut tempat komunikasi secara synchronus
* Apabila Data Channelnya tidak digunakan, maka akan menghasilkan error.
* Secara Default channel hanya bisa menampung 1 data
* Jika ada banyak data yang mau disimpan kedalam sebuah Channel, maka data yang belum masuk ke channel akan menunggu sampai data yang ada didalam channel diambil
* Channel juga hanya bisa menerima 1 tipe data
* Channel direkomendasikan untuk di close apabila sudah dipakai, hal ini untuk mencegah terjadinya memory leak
* Channel dideklarasikan dengan chan
* Cara membuat Channel adalah dengan fungsi make. Contoh make(chan int)
 */
func TestCreateChannel(t *testing.T) {
	channel := make(chan string) // * Cara Membuat Channel

	defer close(channel) // * Cara Close Channel

	// * Pengisian value channel harus didalam sebuah proses goroutine. Apabila tidak maka sistem akan stuck karena go menunggu si channel untuk digunakan
	go func() {
		time.Sleep(2 * time.Second)

		channel <- "Eko" // * Cara Memasukan Value / Data Ke Channel
		fmt.Println("Selesai Mengirim Data Ke Channel")
	}()

	data := <-channel // * Cara Mengambil Value Dari Sebuah Channel
	fmt.Println(data)

	time.Sleep(5 * time.Second)
}

/*
* Channel juga bisa digunakan sebagai parameter sebuah fungsi
* Perlu diingat kalau Golang itu dalam menangani nilai parameter adalah dengan cara Pass By Value (Nilainya diduplikasi dan disimpan di lokasi memori lain)
* Biasanya yang sudah kita lakukan di Dasar Golang, Apabila nilai yang kita isi ke parameter sebuah fungsi ikutan berubah ketika diproses di fungsi terkait, kita harus menggunakan Pointer agar nilai yang masuk ke parameter memiliki sifat Pass By Reference
* Hal diatas tidak berlaku untuk Channel. Jadi kita tidak perlu menggunakan Pointer apabila kita ingin mengisi dan mengambil nilai dari sebuah channel di masing-masing fungsi yang berbeda
 */
func GiveMeResponse(channel chan string) {
	time.Sleep(2 * time.Second)

	channel <- "Erlangga Laimena"
}

func TestChannelAsParameter(t *testing.T) {
	channel := make(chan string)

	defer close(channel)

	go GiveMeResponse(channel)

	fmt.Println(<-channel) // * Apabila Ingin Mengambil Value Channel Tanpa Harus Ditampung Dulu Di Variable
}

/*
* Channel memiliki Konsep In dan Out
* Sebetulnya Channel sebagai parameter sendiri sebetulnya hanya bisa melakukan 1 proses saja didalam sebuah fungsi. Entah dia mengisi value channel atau mengambil value channel
* Dengan konsep In dan Out, diharapkan lebih mempertegas saja kalau fungsi yang kita panggil apakah hanya bisa mengisi atau mengambil value
 */
func OnlyIn(channel chan<- string) {
	time.Sleep(2 * time.Second)

	channel <- "Erlangga Laimena"
}

func OnlyOut(channel <-chan string) {
	data := <-channel

	fmt.Println(data)
}

func TestChannelInAndOut(t *testing.T) {
	channel := make(chan string)

	defer close(channel)

	go OnlyIn(channel)
	go OnlyOut(channel)

	/*
	* Perlu dikasih sleep disini dikarenakan isi value channel diakses di fungsi yang dijalankan secara goroutine.
	* Berangkat dari yang sudah dipelajari, Golang tidak peduli goroutine masih berjalan atau sudah selesai, apabila proses eksekusi fungsi sudah sampai baris terakhir, maka program akan selesai, diterminate dan segala macam variabel akan dihapus dari memori
	* Bila saja pengambilan value channel ada pada fungsi ini, maka program akan menunggu karena seperti yang sudah kita pelajari juga, bila kita sudah menggunakan channel, maka Golang akan melakukan blocking sampai channel terkait kosong lagi
	 */
	time.Sleep(5 * time.Second)
}

/*
* Secara default, Channel pada Golang hanya bisa menerima 1 data saja.
* Namun pada Golang pula terdapat fitur yang bisa membuat channel bisa menerima lebih dari 1 data.
* Fitur ini dinamakan Buffered Channel.
* Cara kerjanya secara garis besar, selama Kapasitas Channel belum terisi semua, maka Golang tidak akan melakukan Blocking terhadap jalannya eksekusi sebuah fungsi.
* Apabila kita memasukan data ke channel yang kapasitasnya sudah penuh, Golang baru melakukan Blocking sampai 1 data Channel diambil lalu dimasukan lagi data baru tadi ke dalam Channel.
 */
func TestBufferedChannel(t *testing.T) {
	channel := make(chan string, 3) // * Cara membuat sebuah channel dengan kapasitas buffer-nya

	channel <- "Erlangga"

	fmt.Println("Selesai") // * tidak error sesuai penjalasan diatas apabila channel mendeklarasikan buffer, maka Golang tidak akan melakukan blocking sampai pada saat kita mau memasukan value baru ke buffered channel yang sudah terisi penuh kapasitasnya

	channel <- "Laimena"
	channel <- "Bisa"

	fmt.Println("Selesai 2")

	// channel <- "Bisa2" // ! Error Kapasitas sudah Penuh dan akan terjadi blocking sampai salah satu value existing di channel digunakan

	fmt.Println("Kapasitas Channel:", cap(channel))
	fmt.Println("Panjang Channel Saat Ini:", len(channel))
	fmt.Println(<-channel)

	channel <- "Golang"

	fmt.Println(<-channel)
	fmt.Println(<-channel)
	fmt.Println(<-channel)

	// fmt.Println(<-channel) // ! Error Karena Golang akan menunggu nilai dari channel terisi yang sebenarnya tidak akan pernah terisi
}

func TestRangeChannel(t *testing.T) {
	channel := make(chan string)

	go func() {
		for i := 0; i < 10; i++ {
			channel <- "Perulangan ke " + strconv.Itoa(i)
		}

		/*
		* Kenapa Proses close channelnya disini?
		* Pertama agar looping yang terjadi dibawah selesai. Apabila tidak kita close channelnya, looping akan terus berjalan dan apabila habis isi value dari channel, maka looping dibawah akan menunggu sampai channelnya diisi kembali
		* Kedua karena ini anonymous function yang dijalankan secara Goroutine(Async) maka proses looping print dibawah akan berjalan tanpa menunggu proses looping di anonymous function ini selesai karena sifat dari Async-nya
		* Sering di mention juga kalau channel setelah diisi, maka dia akan menunggu untuk di ambil isinya kembali. Jadi menyebabkan semacam Blocking oleh Golang. Maka dari itu kode ini tidak error meskipun kita tidak memakai buffered channel
		* Karena sebetulnya setelah perulangan 1 diatas diisi, maka channelnya akan dieksekusi dibawah untuk di print. Barulah perulangan kedua terjadi dan seterusnya
		 */
		close(channel)
	}()

	for data := range channel {
		fmt.Println("Menerima Data " + data)
	}
}

/*
* Fitur Select Channel pada Golang ini sangat berguna apabila kita bekerja dengan lebih dari 1 channel dan value dari semua channel dipakai semua.
 */
func TestSelectChannel(t *testing.T) {
	channel1 := make(chan string)
	channel2 := make(chan string)

	defer close(channel1)
	defer close(channel2)

	go GiveMeResponse(channel1)
	go GiveMeResponse(channel2)

	counter := 0

	for {
		select {
		case data := <-channel1:
			fmt.Println("Data dari channel 1", data)
			counter++
		case data := <-channel2:
			fmt.Println("Data dari channel 2", data)
			counter++
		}

		if counter == 2 {
			break
		}
	}
}

/*
* Pada fitur Select Channel juga terdapat fungsi default.
* Fungsi default ini akan dijalankan terus apabila isi dari channel 1 atau channel 2 belum terisi.
* Fungsi ini sangat berguna apabila kita ingin mengeksekusi / melakukan sesuatu ketika channel masih belum terisi didalam select
 */
func TestDefaultSelectChannel(t *testing.T) {
	channel1 := make(chan string)
	channel2 := make(chan string)

	defer close(channel1)
	defer close(channel2)

	go GiveMeResponse(channel1)
	go GiveMeResponse(channel2)

	counter := 0

	for {
		select {
		case data := <-channel1:
			fmt.Println("Data dari channel 1", data)
			counter++
		case data := <-channel2:
			fmt.Println("Data dari channel 2", data)
			counter++
		default:
			fmt.Println("Menunggu Data")
		}

		if counter == 2 {
			break
		}
	}
}
