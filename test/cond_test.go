package test

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

var locker = sync.Mutex{}

/*
* Cond adalah salah satu struct dari package sync yang berfungsi untuk implementasi locking namun berbasi kondisi
* Implementasi Cond membutuhkan objek / struct Locker (bisa Mutex / RW Mutex) untuk implementasi Lockingnya
* Berbeda dengan fungsi Locking biasanya, terdapat fungsi Wait yang fungsinya menunggu apakah sebuah proses perlu dilanjutkan eksekusinya atau tidak
* Untuk memberitahu / notifikasi Cond untuk melanjutkan sebuah proses Goroutine, terdapat 2 fungsi yang bisa digunakan yaitu Signal() dan Broadcast()
* Signal() berfungsi untuk memberi tahu Cond untuk menghentikan proses Wait dan melanjutkan proses Goroutine terhadap satu proses Goroutine diantara kumpulan proses yang berjalan berbarengan
* Broadcast() berfungsi untuk memberi tahu Cond untuk menghentikan proses Wait dan melanjutkan proses Goroutine terhadap semua proses Goroutine yang berjalan berbarengan
 */
var condition = sync.NewCond(&locker)

var group = sync.WaitGroup{}

func WaitCondition(value int) {
	defer group.Done()
	group.Add(1)

	condition.L.Lock()
	condition.Wait()

	fmt.Println("Done", value)

	condition.L.Unlock()
}

func TestCondition(t *testing.T) {
	for i := 0; i < 10; i++ {
		go WaitCondition(i)
	}

	// go func() {
	// 	for i := 0; i < 10; i++ {
	// 		time.Sleep(1 * time.Second)

	// 		condition.Signal()
	// 	}
	// }()

	go func() {
		time.Sleep(1 * time.Second)

		condition.Broadcast()
	}()

	group.Wait()
}
