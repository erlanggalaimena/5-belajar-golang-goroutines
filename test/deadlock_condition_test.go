package test

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

type UserBalance struct {
	Name    string
	Balance int
	sync.Mutex
}

func (user *UserBalance) Lock() {
	user.Mutex.Lock()
}

func (user *UserBalance) Unlock() {
	user.Mutex.Unlock()
}

func (user *UserBalance) change(amount int) {
	user.Balance = user.Balance + amount
}

func Transfer(user1 *UserBalance, user2 *UserBalance, amount int) {
	user1.Lock()
	fmt.Println("Lock User 1:", user1.Name)
	user1.change(-amount)

	time.Sleep(1 * time.Second)

	user2.Lock()
	fmt.Println("Lock User 2:", user2.Name)
	user2.change(amount)

	time.Sleep(1 * time.Second)

	user1.Unlock()
	user2.Unlock()
}

func TestDeadlock(t *testing.T) {
	user1 := UserBalance{
		Name:    "Eko",
		Balance: 1000000,
	}

	user2 := UserBalance{
		Name:    "Budi",
		Balance: 1000000,
	}

	/*
	* Meskipun tidak terlalu terlihat, namun sebetulnya terjadi deadlock yang mengakibatkan saldo user1 dan user2 hasil akhir amountnya tidak jadi 1.100.000 dan 900.000
	 */
	go Transfer(&user1, &user2, 100000)
	go Transfer(&user2, &user1, 200000)

	time.Sleep(3 * time.Second)

	fmt.Println("User", user1.Name, ", Balance", user1.Balance)
	fmt.Println("User", user2.Name, ", Balance", user2.Balance)
}
