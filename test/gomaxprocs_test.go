package test

import (
	"fmt"
	"runtime"
	"sync"
	"testing"
	"time"
)

func TestGomaxprocs(t *testing.T) {
	waitGroup := sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		waitGroup.Add(1)

		go func() {
			defer waitGroup.Done()

			time.Sleep(3 * time.Second)
		}()
	}

	totalCpu := runtime.NumCPU()
	totalThread := runtime.GOMAXPROCS(-1)
	totalGoroutine := runtime.NumGoroutine()

	fmt.Println("Total CPU", totalCpu)
	fmt.Println("Total Thread", totalThread)
	fmt.Println("Total Goroutine", totalGoroutine)

	waitGroup.Wait()
}
