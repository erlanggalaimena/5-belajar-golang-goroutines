package test

import (
	"belajar-golang-goroutines/service"
	"fmt"
	"testing"
	"time"
)

/*
* Secara default Go akan menjalankan semua prosesnya dengan cara Concurrency
* Apa itu Concurrency?
* Concurrency adalah konsep dimana multiple / beberapa proses dilakukan secara bergantian
* Perbedaannya dengan Parallel?
* Parallel adalah konsep dimana multiple / beberapa proses dilakukan secara bersamaan dengan dibagi ke beberapa segmentasi (Thread)
* Jadi kesimpulannya Parallel menjalankan beberapa fungsi dalam waktu yang bersamaan sedangkan Concurrency menjalankan beberapa fungsi namun dilakukan secara bergantian
* Goroutines sendiri berfungsi untuk menjalankan proses didalam sebuah thread kecil, yang sebenarnya thread kecil tersebut berjalan di thread sebenarnya.
* Perbedaan thread kecil yang dibuat oleh Go dengan Thread sebenarnya adalah pada masalah ukuran. Untuk thread kecil ukuran memorynya kisaran 2 kb sedangkan thread sebenarnya bisa jadi di kisaran 1Mb
* Didalam Goroutine sendiri nantinya akan dijalankan concurrency juga menyebabkan apabila didalam sebuah thread kecil terdapat beberapa proses, sebetulnya Go menjalankan programnya tidak beriringan antara proses
* Namun dijalankan secara Concurrency / bergantian dimana kalau proses yang saat ini sedang dijalankan lambat, maka go akan menjalankan proses selanjutnya yang ada pada antrian dan seterusnya sampai semua proses akhirnya selesai
* Implementasi dari goroutines sangat sederhana yaitu menuliskan tulisan "go" sebelum memanggil sebuah function
* Namun perlu diketahui kalau goroutines ini lebih efektif kalau dipergunakan untuk fungsi yang tidak memiliki return value / balikan meskipun sebenarnya bisa
* Alasannya adalah hasil dari fungsi yang menggunakan goroutines tidak bisa ditampung
* Goroutines juga bisa dianalogikan sebagai asynchronous
 */
func TestCreateGoroutine(t *testing.T) {
	go service.HelloWorld("Angga")

	fmt.Println("Ups")

	time.Sleep(1 * time.Second)
}

func TestManyGoroutine(t *testing.T) {
	for i := 0; i < 100000; i++ {
		go service.DisplayNumber(i)
	}

	time.Sleep(5 * time.Second)
}
