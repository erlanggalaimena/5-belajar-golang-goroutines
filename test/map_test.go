package test

import (
	"fmt"
	"strconv"
	"sync"
	"testing"
)

func AddToMap(waitGroup *sync.WaitGroup, data *sync.Map, key int, value int) {
	defer waitGroup.Done()

	waitGroup.Add(1)

	data.Store(key, value)
}

func TestMap(*testing.T) {
	data := &sync.Map{}

	waitGroup := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		go AddToMap(waitGroup, data, i, i)
	}

	waitGroup.Wait()

	data.Range(func(key, value any) bool {
		fmt.Println(strconv.Itoa(key.(int))+":", value)

		return true
	})
}
