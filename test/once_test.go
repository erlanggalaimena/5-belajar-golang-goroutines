package test

import (
	"fmt"
	"sync"
	"testing"
)

var counter = 0

func OnlyOnce() {
	counter++
}

/*
* Struct Once digunakan jika dan hanya jika kita hanya ingin mengeksekusi 1 proses goroutine saja
* Dengan bantuan struct Once, apabila kita menjalan goroutine lebih dari 1 proses diwaktu yang berdekatan, maka hanya goroutine tercepat saja dia yang diproses, sisanya akan ditolak / diabaikan untuk di proses
 */
func TestOnce(t *testing.T) {
	var group sync.WaitGroup = sync.WaitGroup{}

	var once sync.Once = sync.Once{}

	for i := 0; i < 100; i++ {
		group.Add(1)

		once.Do(OnlyOnce)

		group.Done()
	}

	group.Wait()

	fmt.Println("Counter:", counter)
}
