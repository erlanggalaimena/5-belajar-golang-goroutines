package test

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	var pool sync.Pool = sync.Pool{}
	pool.Put("Khafizyan")
	pool.Put("Naim")
	pool.Put("Laimena")

	var group sync.WaitGroup = sync.WaitGroup{}

	for i := 0; i < 10; i++ {
		group.Add(1)

		go func() {
			defer group.Done()

			name := pool.Get()

			fmt.Println(name)

			pool.Put(name)
		}()
	}

	group.Wait()

	fmt.Println("Selesai")
}

func TestPoolWithDefaultValue(t *testing.T) {
	var pool sync.Pool = sync.Pool{
		New: func() any {
			return "Default Value"
		},
	}

	pool.Put("Erlangga")
	pool.Put("Laimena")

	var waitGroup sync.WaitGroup = sync.WaitGroup{}

	for i := 0; i < 10; i++ {
		waitGroup.Add(1)

		go func() {
			defer waitGroup.Done()

			value := pool.Get()

			fmt.Println(value)

			time.Sleep(1 * time.Second)

			pool.Put(value)
		}()
	}

	waitGroup.Wait()

	fmt.Println("Selesai")
}
