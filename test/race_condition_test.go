package test

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

/*
* Contoh Race Condition yang bisa terjadi pada Goroutine
 */
func TestRaceCondition(t *testing.T) {
	x := 0

	for i := 0; i < 1000; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				x++
			}
		}()
	}

	time.Sleep(5 * time.Second)

	fmt.Println("Counter:", x)
}

/*
* Salah satu cara untuk menanggulangi masalah race condition yang bisa terjadi pada Goroutine adalah dengan cara menggunakan struct / objek bernama mutex yang ada pada package sync
* didalam struct mutex, terdapat fungsi Lock yang berfungsi untuk mengunci eksekusi sebuah kode program dan Unlock yang berfungsi untuk membuka kuncinya
 */
func TestMutex(t *testing.T) {
	x := 0

	var mutex sync.Mutex

	for i := 0; i < 1000; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				mutex.Lock()
				x++
				mutex.Unlock()
			}
		}()
	}

	time.Sleep(5 * time.Second)

	fmt.Println("Counter:", x)
}

type BankAccount struct {
	Balance int
	RWMutex sync.RWMutex
}

func (account *BankAccount) addBalance(amount int) {
	account.RWMutex.Lock()
	account.Balance += amount
	account.RWMutex.Unlock()
}

func (account *BankAccount) getBalance() int {
	account.RWMutex.RLock()
	balance := account.Balance
	account.RWMutex.RUnlock()

	return balance
}

/*
* RWMutex juga merupakan salah satu struct yang ada pada package sync
* Fungsinya secara garis besar sama dengan Mutex hanya saja ada tambahan untuk Lock dan Unlock proses Read
 */
func TestRWMutex(t *testing.T) {
	account := BankAccount{}

	for i := 0; i < 100; i++ {
		go func() {
			for j := 0; j < 100; j++ {
				account.addBalance(1)
				fmt.Println(account.getBalance())
			}
		}()
	}

	time.Sleep(5 * time.Second)
}
