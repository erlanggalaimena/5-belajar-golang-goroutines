package test

import (
	"fmt"
	"testing"
	"time"
)

func TestTicker(t *testing.T) {
	var ticker = time.NewTicker(1 * time.Second)

	go func() {
		time.Sleep(5 * time.Second)

		ticker.Stop()
	}()

	for time := range ticker.C {
		fmt.Println(time)
	}
}

func TestTick(t *testing.T) {
	var channel = time.Tick(1 * time.Second)

	for time := range channel {
		fmt.Println(time)
	}
}
