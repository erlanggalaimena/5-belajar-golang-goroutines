package test

import (
	"fmt"
	"sync"
	"testing"
	"time"
)

/*
* Timer merupakan representasi dari sebuah kejadian yang berjalan dalam jangka waktu yang sudah ditentukan
* Ketika kita pertama kali membuat sebuah struct Timer, kita akan selalu diminta untuk mendefinisikan berapa lama waktu untuk menghentikan operasi
* Ketika operasi selesai, maka event akan dikirimkan ke dalam Channel yang dimiliki struct Timer
 */
func TestTimer(t *testing.T) {
	var timer = time.NewTimer(5 * time.Second)

	fmt.Println(time.Now())

	time := <-timer.C
	fmt.Println(time)
}

/*
* Apabila kita mau langsung mendapatkan informasi dari channel Timer tanpa kita harus mendapatkan struct Timer, bisa digunakan fungsi After
* Fungsi After mengembalikan nilai channel setelah proses selesai
 */
func TestAfter(t *testing.T) {
	channel := time.After(5 * time.Second)

	fmt.Println(time.Now())

	time := <-channel
	fmt.Println(time)
}

/*
* Apabila ada kebutuhan untuk menjalankan function dengan delay tertentu, kita bisa memanfaatkan Timer dengan menggunakan function AfterFunc dari package time
* Balikan dari fungsinya adalah pointer Timer
* Dengan menggunakan AfterFunc kita tidak perlu lagi harus menampung value channel dan menggunakannya agar function tidak terjadi deathlock
 */
func TestAfterFunc(t *testing.T) {
	waitGroup := sync.WaitGroup{}
	waitGroup.Add(1)

	time.AfterFunc(5*time.Second, func() {
		defer waitGroup.Done()

		fmt.Println("Run After 5 Seconds")
		fmt.Println(time.Now())
	})

	fmt.Println(time.Now())

	waitGroup.Wait()
}
