package test

import (
	"fmt"
	"sync"
	"testing"
)

/*
* Sebelumnya kita untuk menunggu proses Goroutine selesai menggunakan fungsi Sleep pada package time
* Namun cara tersebut kurang diasarankan
* Golang menyediakan fungsi yang lebih proper untuk menunggu proses dari Goroutine selesai.
* Salah satunya yaitu menggunakan struct WaitGroup yang ada pada package sync
* Konsep kerjanya cukup sederhana:
* 1. Ketika kita sudah goroutine, panggil fungsi Add yang ada pada struct WaitGroup. Fungsi Add menerima masukan tipe int yang mendeskripsikan counter / jumlah proses
* 2. Ketika fungsi yang dijalankan menggunakan gorotuine selesai, panggil fungsi Done yang ada pada struct WaitGroup. Fungsi Done fungsinya untuk mengurangi counter / jumlah proses
* 3. Ketiga panggil fungsi Wait yang ada pada struct WaitGroup untuk menunggu hinggal struct WaitGroup menyatakan selesai. Dikatakan selesai kalau nilai counter yang dimiliki struct WaitGroup bernilai 0
 */
func RunAsynchronous(group *sync.WaitGroup) {
	defer group.Done()

	group.Add(1)

	fmt.Println("Hello World")
}

func TestWaitGroup(t *testing.T) {
	waitGroup := &sync.WaitGroup{}

	for i := 0; i < 100; i++ {
		go RunAsynchronous(waitGroup)
	}

	waitGroup.Wait()

	fmt.Println("Process Done")
}
